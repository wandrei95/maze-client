package com.example.andreiwegroszta.myapplication.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Question;
import com.example.andreiwegroszta.myapplication.maze_view.MazeView;

public class MazeActivity extends AppCompatActivity implements MazeView.OnMazeSolvedListener, View.OnClickListener {
    public static final String QUESTION_KEY = "QUESTION";
    private MazeView mazeView;

    private boolean isSolutionShown = false;
    private Question question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maze);

        mazeView = findViewById(R.id.maze);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(QUESTION_KEY)) {
            question = (Question) bundle.getSerializable(QUESTION_KEY);
        }

        findViewById(R.id.btn_show_solution).setOnClickListener(this);
        findViewById(R.id.btn_get_maze).setOnClickListener(this);
        mazeView.setOnMazeSolvedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_get_maze:
                if (question != null) {
                    int n = question.getDifficulty();
                    mazeView.generateMaze(n, n);
                    isSolutionShown = false;
                }
                break;
            case R.id.btn_show_solution:
                if (!isSolutionShown) {
                    mazeView.showHelper();
                } else {
                    mazeView.hideHelper();
                }
                isSolutionShown = !isSolutionShown;
                break;
        }
    }

    @Override
    public void onMazeSolved() {
        Intent returnIntent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(QUESTION_KEY, question);
        returnIntent.putExtras(bundle);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
