package com.example.andreiwegroszta.myapplication.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Question;

public class AnswerActivity extends AppCompatActivity {
    public static final String QUESTION_KEY = "QUESTION";

    private TextView tvQuestion;
    private TextView tvAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);

        tvQuestion = findViewById(R.id.tv_question);
        tvAnswer = findViewById(R.id.tv_answer);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.containsKey(QUESTION_KEY)) {
            Question question = (Question) bundle.getSerializable(QUESTION_KEY);
            tvQuestion.setText(question.getText());
            tvAnswer.setText(question.getAnswer());
            Toast.makeText(this, question.getAnswer(), Toast.LENGTH_LONG).show();
        }
    }
}
