package com.example.andreiwegroszta.myapplication.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.example.andreiwegroszta.myapplication.R;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivSatan;
    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initViews();

        animSatan();

        animStartButton();
    }

    private void initViews() {
        ivSatan = findViewById(R.id.iv_satan);
        btnStart = findViewById(R.id.btn_start);
        btnStart.setOnClickListener(this);
    }


    private void animSatan() {
        Animation satanAnim = AnimationUtils.loadAnimation(this, R.anim.satan_anim);
        ivSatan.startAnimation(satanAnim);
    }

    private void animStartButton() {
        Animation startBtnAnimation = AnimationUtils.loadAnimation(this, R.anim.from_bottom);
        btnStart.startAnimation(startBtnAnimation);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                goToMain();
                finish();
                break;
        }
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
