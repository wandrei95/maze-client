package com.example.andreiwegroszta.myapplication.ui.questions;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Question;

import java.util.ArrayList;
import java.util.List;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionViewHolder> {
    private final List<Question> questions;
    private OnQuestionClickListener listener;

    public QuestionsAdapter(List<Question> questions) {
        this.questions = new ArrayList<>(questions);
    }

    public QuestionsAdapter() {
        this.questions = new ArrayList<>();
    }

    private void setCategories(List<Question> questions) {
        this.questions.clear();
        this.questions.addAll(questions);
    }

    public void setOnQuestionClickListener(OnQuestionClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.question_layout, viewGroup, false);
        return new QuestionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder questionViewHolder, int i) {
        Question question = questions.get(i);
        questionViewHolder.tvQuestion.setText(question.getText());
        questionViewHolder.container.setOnClickListener(view -> {
            if (listener != null) {
                listener.onQuestionClicked(question);
            }
        });
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    class QuestionViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestion;
        private RelativeLayout container;

        public QuestionViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQuestion = itemView.findViewById(R.id.tv_question);
            container = itemView.findViewById(R.id.container);
        }
    }

    interface OnQuestionClickListener {
        void onQuestionClicked(Question question);
    }
}
