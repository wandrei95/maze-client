package com.example.andreiwegroszta.myapplication.ui.categories;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Category;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;


public class CategoriesFragment extends Fragment {
    private RecyclerView rvCategories;

    private CategoriesAdapter categoriesAdapter;
    private CategorySelectedListener listener;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    public void setCategorySelectedListener(CategorySelectedListener listener) {
        this.listener = listener;
    }

    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        rvCategories = view.findViewById(R.id.rv_categories);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCategories.setLayoutManager(layoutManager);

        String json = getData();
        Type listType = new TypeToken<List<Category>>() {
        }.getType();
        List<Category> categories = new Gson().fromJson(json, listType);

        categoriesAdapter = new CategoriesAdapter(categories);
        rvCategories.setAdapter(categoriesAdapter);

        categoriesAdapter.setOnCategoryClickListener(category -> {
            if (listener != null) {
                listener.onCategorySelected(category);
            }
        });
        return view;
    }

    private String getData() {
        StringBuilder text = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(getActivity().getAssets().open("data.json")))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }

    public interface CategorySelectedListener {
        void onCategorySelected(Category category);
    }

}
