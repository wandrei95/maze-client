package com.example.andreiwegroszta.myapplication.maze_view.maze;

public class MazeCell {
    //the first index in the maze cell matrix
    private final int x;
    //the first index in the maze cell matrix
    private final int y;
    private boolean top;
    private boolean bottom;
    private boolean right;
    private boolean left;
    private boolean visited;

    public MazeCell(int x, int y) {
        this.x = x;
        this.y = y;
        top = true;
        bottom = true;
        right = true;
        left = true;
        visited = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public boolean isBottom() {
        return bottom;
    }

    public void setBottom(boolean bottom) {
        this.bottom = bottom;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
