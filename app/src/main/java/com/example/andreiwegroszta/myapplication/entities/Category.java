package com.example.andreiwegroszta.myapplication.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable{
    private String id;
    private String name;
    private List<Question> questions;

    public Category() {
    }

    public Category(String id, String name, List<Question> questions) {
        this.id = id;
        this.name = name;
        this.questions = new ArrayList<>();
        this.questions.addAll(questions);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Question> getQuestions() {
        return new ArrayList<>(questions);
    }

    public void setQuestions(List<Question> questions) {
        this.questions.clear();
        this.questions.addAll(questions);
    }
}
