package com.example.andreiwegroszta.myapplication.entities;

import java.io.Serializable;

public class Question implements Serializable {
    private String id;
    private String categoryId;
    private String text;
    private String answer;
    private int difficulty;


    public Question() {
    }

    public Question(String id, String categoryId, String text, String answer, int difficulty) {
        this.id = id;
        this.categoryId = categoryId;
        this.text = text;
        this.answer = answer;
        this.difficulty = difficulty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
}
