package com.example.andreiwegroszta.myapplication.maze_view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.andreiwegroszta.myapplication.maze_view.maze.MazeCell;
import com.example.andreiwegroszta.myapplication.maze_view.maze.MazeGenerator;
import com.example.andreiwegroszta.myapplication.maze_view.solve.MazeSolver;
import com.example.andreiwegroszta.myapplication.maze_view.solve.SolveMazeCell;

import java.util.List;


//x, y drawing pairs associate with j, i matrix pairs
// →x ↓y
// →j ↓i
public class MazeView extends View {
    private static final int OFFSET = 30;
    private static final float TOUCH_TOLERANCE = 4;
    public static final int PADDING = 60;

    private Paint mazePaint;
    private Paint helperPaint;
    private Paint drawingPaint;
    private Path drawingPath;
    private Canvas pathCanvas;
    private Canvas mazeCanvas;
    private Canvas helperCanvas;
    private Bitmap pathBitmap;
    private Bitmap mazeBitmap;
    private Bitmap helperBitmap;

    private float previousX;
    private float previousY;

    private int horizontalLineSize;
    private int verticalLineSize;

    private MazeCell[][] maze;
    private SolutionCell[][] solution;
    private int numberOfColumns;
    private int numberOfRows;

    private List<SolveMazeCell> helperSolution;
    private boolean shouldShowHelper;

    private OnMazeSolvedListener listener;
    private boolean shouldNotifyListener = true;


    public MazeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    private void init() {
        drawingPath = new Path();

        setupHelperPaint();

        setupMazePaint();

        setupDrawingPaint();
    }

    private void setupHelperPaint() {
        helperPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        helperPaint.setColor(Color.GREEN);
    }

    private void setupMazePaint() {
        mazePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mazePaint.setColor(Color.WHITE);
        mazePaint.setStrokeWidth(5);
    }

    private void setupDrawingPaint() {
        drawingPaint = new Paint();
        drawingPaint.setDither(true);
        drawingPaint.setAntiAlias(true);
        drawingPaint.setColor(Color.RED);
        drawingPaint.setStrokeJoin(Paint.Join.ROUND);
        drawingPaint.setStyle(Paint.Style.STROKE);
        drawingPaint.setStrokeCap(Paint.Cap.ROUND);
        drawingPaint.setStrokeWidth(5);
    }

    public void setOnMazeSolvedListener(OnMazeSolvedListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onSizeChanged(int width, int height,
                                 int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        pathBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        pathCanvas = new Canvas(pathBitmap);

        mazeBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mazeCanvas = new Canvas(mazeBitmap);

        helperBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        helperCanvas = new Canvas(helperBitmap);
    }

    public void generateMaze(int numberOfRows, int numberOfColumns) {
        generateNewMaze(numberOfRows, numberOfColumns);

        clearDrawings();

        if (maze != null) {
            solution = new SolutionCell[numberOfRows][numberOfColumns];
            setupNewMaze();
        }

        drawingPath.reset();
        invalidate();
    }

    public void showHelper() {
        shouldShowHelper = true;
        invalidate();
    }

    public void hideHelper() {
        shouldShowHelper = false;
        invalidate();
    }

    private void generateNewMaze(int numberOfRows, int numberOfColumns) {
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
        shouldNotifyListener = true;

        MazeGenerator mazeGenerator = new MazeGenerator(numberOfRows, numberOfColumns);
        maze = mazeGenerator.createMaze();

        MazeSolver mazeSolver = new MazeSolver(mazeGenerator);
        helperSolution = mazeSolver.generateGraph();

    }

    private void clearDrawings() {
        mazeCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        pathCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        helperCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        shouldShowHelper = false;
    }

    private void setupNewMaze() {
        computeMazeCellDrawingDimens();

        setMazeStartAndFinish();

        drawMazeOnMazeCanvas();
    }

    private void computeMazeCellDrawingDimens() {
        horizontalLineSize = (getWidth() - PADDING) / numberOfColumns;
        verticalLineSize = (getHeight() - PADDING) / numberOfRows;
    }

    private void setMazeStartAndFinish() {
        maze[numberOfRows - 1][numberOfColumns - 1].setRight(false);
        maze[0][0].setLeft(false);
    }

    private void drawMazeOnMazeCanvas() {
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                MazeCell mazeCell = maze[i][j];
                drawMazeCellWalls(i, j, mazeCell);
            }
        }

        drawHelper();
    }

    private void drawHelper() {
        for (SolveMazeCell sc : helperSolution) {
            float cx = computeX(sc.getMazeCell().getY()) + horizontalLineSize / 2;
            float cy = computeY(sc.getMazeCell().getX()) + verticalLineSize / 2;
            helperCanvas.drawCircle(cx, cy, horizontalLineSize / 10, helperPaint);
        }
    }

    private void drawMazeCellWalls(int mazeMatrixI, int mazeMatrixJ, MazeCell mazeCell) {
        drawMazeCellTopWall(mazeMatrixI, mazeMatrixJ, mazeCell);
        drawMazeCellBottomWall(mazeMatrixI, mazeMatrixJ, mazeCell);
        drawMazeCellRightWall(mazeMatrixI, mazeMatrixJ, mazeCell);
        drawMazeCellLeftWall(mazeMatrixI, mazeMatrixJ, mazeCell);
    }

    private void drawMazeCellTopWall(int mazeMatrixI, int mazeMatrixJ, MazeCell mazeCell) {
        if (mazeCell.isTop()) {
            mazeCanvas.drawLine(computeX(mazeMatrixJ), computeY(mazeMatrixI),
                    computeX(mazeMatrixJ + 1), computeY(mazeMatrixI), mazePaint);
        }
    }

    private void drawMazeCellBottomWall(int mazeMatrixI, int mazeMatrixJ, MazeCell mazeCell) {
        if (mazeCell.isBottom()) {
            mazeCanvas.drawLine(computeX(mazeMatrixJ + 1), computeY(mazeMatrixI + 1),
                    computeX(mazeMatrixJ), computeY(mazeMatrixI + 1), mazePaint);
        }
    }

    private void drawMazeCellRightWall(int mazeMatrixI, int mazeMatrixJ, MazeCell mazeCell) {
        if (mazeCell.isRight()) {
            mazeCanvas.drawLine(computeX(mazeMatrixJ + 1), computeY(mazeMatrixI),
                    computeX(mazeMatrixJ + 1), computeY(mazeMatrixI + 1), mazePaint);
        }
    }

    private void drawMazeCellLeftWall(int mazeMatrixI, int mazeMatrixJ, MazeCell mazeCell) {
        if (mazeCell.isLeft()) {
            mazeCanvas.drawLine(computeX(mazeMatrixJ), computeY(mazeMatrixI),
                    computeX(mazeMatrixJ), computeY(mazeMatrixI + 1), mazePaint);
        }
    }

    private float computeX(int position) {
        return OFFSET + position * horizontalLineSize;
    }

    private float computeY(int position) {
        return OFFSET + position * verticalLineSize;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setBackgroundColor(Color.BLACK);

        canvas.drawBitmap(mazeBitmap, 0, 0, null);
        canvas.drawBitmap(pathBitmap, 0, 0, null);
        if (shouldShowHelper) {
            canvas.drawBitmap(helperBitmap, 0, 0, null);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchStart(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touchUp();
                break;
        }
        return true;
    }

    private void touchStart(float x, float y) {
        drawingPath.moveTo(x, y);
        previousX = x;
        previousY = y;
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - previousX);
        float dy = Math.abs(y - previousY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            drawingPath.quadTo(previousX, previousY,
                    (x + previousX) / 2, (y + previousY) / 2);

            previousX = x;
            previousY = y;

            pathCanvas.drawPath(drawingPath, drawingPaint);
        }

        visitCell(x, y);
        checkForWinner();
    }

    private void visitCell(float x, float y) {
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                if (isPointInsideMazeCell(x, y, i, j)) {
                    MazeCell mazeCell = maze[i][j];
                    solution[i][j] = new SolutionCell(mazeCell);
                }
            }
        }
    }

    private boolean isPointInsideMazeCell(float x, float y, int mazeMatrixI, int mazeMatrixJ) {
        int minX = (int) computeX(mazeMatrixJ);
        int maxX = (int) computeX(mazeMatrixJ + 1);
        int minY = (int) computeY(mazeMatrixI);
        int maxY = (int) computeY(mazeMatrixI + 1);

        return x <= mazeCanvas.getWidth() && x >= 0
                && x >= minX && x <= maxX
                && y <= mazeCanvas.getHeight() && y >= 0
                && y >= minY && y <= maxY;
    }

    private void checkForWinner() {
        if (listener != null && shouldNotifyListener) {
            if (solution[0][0] != null && solution[numberOfRows - 1][numberOfColumns - 1] != null) {
                if (checkPathToFinish(0, 0)) {
                    shouldNotifyListener = false;
                    listener.onMazeSolved();
                } else {
                    for (SolutionCell[] aSolution : solution) {
                        for (int j = 0; j < numberOfColumns; j++) {
                            if (aSolution[j] != null) {
                                aSolution[j].setVisited(false);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean checkPathToFinish(int mazeMatrixI, int mazeMatrixJ) {
        if (mazeMatrixI >= 0 && mazeMatrixI < numberOfRows && mazeMatrixJ >= 0 && mazeMatrixJ < numberOfColumns) {
            SolutionCell solutionCell = solution[mazeMatrixI][mazeMatrixJ];
            if (solutionCell == null || solutionCell.isVisited()) {
                return false;
            }

            solutionCell.setVisited(true);

            if (mazeMatrixI == numberOfRows - 1 && mazeMatrixJ == numberOfColumns - 1) {
                return true;
            }

            if (areAllSolutionCellsVisited()) {
                return false;
            }
            return (!solutionCell.getMazeCell().isRight() && checkPathToFinish(mazeMatrixI, mazeMatrixJ + 1))
                    || (!solutionCell.getMazeCell().isLeft() && checkPathToFinish(mazeMatrixI, mazeMatrixJ - 1))
                    || (!solutionCell.getMazeCell().isTop() && checkPathToFinish(mazeMatrixI - 1, mazeMatrixJ))
                    || (!solutionCell.getMazeCell().isBottom() && checkPathToFinish(mazeMatrixI + 1, mazeMatrixJ));
        }
        return false;
    }

    private boolean areAllSolutionCellsVisited() {
        for (SolutionCell[] aSolution : solution) {
            for (int j = 0; j < numberOfColumns; j++) {
                if (aSolution[j] != null && !aSolution[j].isVisited()) {
                    return false;
                }
            }
        }
        return true;
    }

    private void touchUp() {
        drawingPath.reset();
    }

    public interface OnMazeSolvedListener {
        void onMazeSolved();
    }
}
