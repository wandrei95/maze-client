package com.example.andreiwegroszta.myapplication.maze_view;

import com.example.andreiwegroszta.myapplication.maze_view.maze.MazeCell;

public class SolutionCell {
    private final MazeCell mazeCell;
    private boolean isVisited;

    public SolutionCell(MazeCell mazeCell) {
        this.mazeCell = mazeCell;
        isVisited = false;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    public MazeCell getMazeCell() {
        return mazeCell;
    }
}
