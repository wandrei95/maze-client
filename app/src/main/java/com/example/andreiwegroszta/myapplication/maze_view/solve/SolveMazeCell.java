package com.example.andreiwegroszta.myapplication.maze_view.solve;

import com.example.andreiwegroszta.myapplication.maze_view.maze.MazeCell;

import java.util.ArrayList;
import java.util.List;

public class SolveMazeCell {
    private final MazeCell mazeCell;
    private final List<SolveMazeCell> children;
    private SolveMazeCell parent;
    private boolean visited;
    private DirectionFromParent directionFromParent;

    public SolveMazeCell(MazeCell mazeCell, SolveMazeCell parent, List<SolveMazeCell> children) {
        this.mazeCell = mazeCell;
        this.parent = parent;
        this.children = children;
    }

    public SolveMazeCell(MazeCell mazeCell, SolveMazeCell parent) {
        this.mazeCell = mazeCell;
        this.parent = parent;
        this.children = new ArrayList<>();
    }


    public void addChild(SolveMazeCell solveMazeCell) {
        children.add(solveMazeCell);
    }

    public MazeCell getMazeCell() {
        return mazeCell;
    }

    public List<SolveMazeCell> getChildren() {
        return children;
    }

    public SolveMazeCell getParent() {
        return parent;
    }

    public void setParent(SolveMazeCell parent) {
        this.parent = parent;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public DirectionFromParent getDirectionFromParent() {
        return directionFromParent;
    }

    public void setDirectionFromParent(DirectionFromParent directionFromParent) {
        this.directionFromParent = directionFromParent;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof SolveMazeCell)) {
            return false;
        }
        SolveMazeCell sc = (SolveMazeCell) obj;

        return mazeCell.getX() == sc.mazeCell.getX()
                && mazeCell.getY() == sc.mazeCell.getY();
    }

    public enum DirectionFromParent {
        TOP, BOTTOM, RIGHT, LEFT;
    }
}
