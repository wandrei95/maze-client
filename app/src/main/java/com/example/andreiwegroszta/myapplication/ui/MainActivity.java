package com.example.andreiwegroszta.myapplication.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Category;
import com.example.andreiwegroszta.myapplication.entities.Question;
import com.example.andreiwegroszta.myapplication.ui.categories.CategoriesFragment;
import com.example.andreiwegroszta.myapplication.ui.questions.QuestionsFragment;

public class MainActivity extends AppCompatActivity implements CategoriesFragment.CategorySelectedListener, QuestionsFragment.QuestionSelectedListener {

    public static final int MAZE_REQUEST_CODE = 12345;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CategoriesFragment categoriesFragment = new CategoriesFragment();
        categoriesFragment.setCategorySelectedListener(this);
        openFragment(categoriesFragment, false);
    }

    @Override
    public void onCategorySelected(Category category) {
        QuestionsFragment questionsFragment = new QuestionsFragment();
        questionsFragment.setQuestionSelectedListener(this);
        Bundle bundle = new Bundle();
        bundle.putSerializable(QuestionsFragment.CATEGORY_KEY, category);
        questionsFragment.setArguments(bundle);
        openFragment(questionsFragment, true);
    }

    @Override
    public void onQuestionSelected(Question question) {
        Intent intent = new Intent(this, MazeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(MazeActivity.QUESTION_KEY, question);
        intent.putExtras(bundle);
        startActivityForResult(intent, MAZE_REQUEST_CODE);
    }

    private void openFragment(final Fragment fragment, final boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MAZE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null && bundle.containsKey(MazeActivity.QUESTION_KEY)) {
                    Question question = (Question) bundle.getSerializable(MazeActivity.QUESTION_KEY);
                    showAnswer(question);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Some questions are better not answered", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showAnswer(Question question) {
        Intent intent = new Intent(this, AnswerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AnswerActivity.QUESTION_KEY, question);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
