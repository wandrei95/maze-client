package com.example.andreiwegroszta.myapplication.maze_view.solve;


import com.example.andreiwegroszta.myapplication.maze_view.maze.MazeCell;
import com.example.andreiwegroszta.myapplication.maze_view.maze.MazeGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class MazeSolver {
    private final MazeCell[][] grid;
    private final int numberOfColumns;
    private final int numberOfRows;
    private List<SolveMazeCell> solution;

    public MazeSolver(MazeGenerator mazeGenerator) {
        grid = mazeGenerator.getGrid();
        numberOfColumns = mazeGenerator.getNumberOfColumns();
        numberOfRows = mazeGenerator.getNumberOfRows();
    }

    public List<SolveMazeCell> generateGraph() {
        if (solution == null) {
            SolveMazeCell[][] solveGrid = createSolveGrid();

            SolveMazeCell destination = addParents(solveGrid[0]);

            solution = generateSolution(destination);
        }
        return solution;
    }

    private SolveMazeCell[][] createSolveGrid() {
        SolveMazeCell[][] solveGrid = new SolveMazeCell[numberOfRows][numberOfColumns];

        createSolveGridWithoutChildren(solveGrid);

        addChildrenToSolveGrid(solveGrid);

        return solveGrid;
    }

    private void createSolveGridWithoutChildren(SolveMazeCell[][] solveGrid) {
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                solveGrid[i][j] = new SolveMazeCell(grid[i][j], null);
            }
        }
    }

    private void addChildrenToSolveGrid(SolveMazeCell[][] solveGrid) {
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                addChildren(solveGrid, i, j, solveGrid[i][j]);
            }
        }
    }

    private SolveMazeCell addParents(SolveMazeCell[] solveMazeCells) {
        Stack<SolveMazeCell> stack = new Stack<>();
        stack.push(solveMazeCells[0]);
        SolveMazeCell current = null;

        while (!stack.isEmpty()) {
            current = stack.pop();
            current.setVisited(true);
            if (isSolveCellDestination(current)) {
                break;
            }

            for (SolveMazeCell sc : current.getChildren()) {
                if (!sc.isVisited()) {
                    sc.setParent(current);
                    stack.push(sc);
                }
            }
        }
        return current;
    }

    private boolean isSolveCellDestination(SolveMazeCell current) {
        return current.getMazeCell().getX() == numberOfRows - 1
                && current.getMazeCell().getY() == numberOfColumns - 1;
    }

    private void addChildren(SolveMazeCell[][] solveGrid, int i, int j, SolveMazeCell solveMazeCell) {
        addChildFromTop(solveGrid, i, j, solveMazeCell);
        addChildFromBottom(solveGrid, i, j, solveMazeCell);
        addChildFromRight(solveGrid, i, j, solveMazeCell);
        addChildFromLeft(solveGrid, i, j, solveMazeCell);
    }

    private void addChildFromTop(SolveMazeCell[][] solveGrid, int i, int j, SolveMazeCell solveMazeCell) {
        if (i + 1 < numberOfRows
                && !solveGrid[i][j].getMazeCell().isBottom()
                && !solveGrid[i + 1][j].getMazeCell().isTop()) {
            solveGrid[i + 1][j].setDirectionFromParent(SolveMazeCell.DirectionFromParent.BOTTOM);
            solveMazeCell.addChild(solveGrid[i + 1][j]);
        }
    }

    private void addChildFromBottom(SolveMazeCell[][] solveGrid, int i, int j, SolveMazeCell solveMazeCell) {
        if (i - 1 >= 0
                && !solveGrid[i][j].getMazeCell().isTop()
                && !solveGrid[i - 1][j].getMazeCell().isBottom()) {
            solveGrid[i - 1][j].setDirectionFromParent(SolveMazeCell.DirectionFromParent.TOP);
            solveMazeCell.addChild(solveGrid[i - 1][j]);
        }
    }

    private void addChildFromRight(SolveMazeCell[][] solveGrid, int i, int j, SolveMazeCell solveMazeCell) {
        if (j + 1 < numberOfColumns
                && !solveGrid[i][j].getMazeCell().isRight()
                && !solveGrid[i][j + 1].getMazeCell().isLeft()) {
            solveGrid[i][j + 1].setDirectionFromParent(SolveMazeCell.DirectionFromParent.RIGHT);
            solveMazeCell.addChild(solveGrid[i][j + 1]);
        }
    }

    private void addChildFromLeft(SolveMazeCell[][] solveGrid, int i, int j, SolveMazeCell solveMazeCell) {
        if (j - 1 >= 0
                && !solveGrid[i][j].getMazeCell().isLeft()
                && !solveGrid[i][j - 1].getMazeCell().isRight()) {
            solveGrid[i][j - 1].setDirectionFromParent(SolveMazeCell.DirectionFromParent.LEFT);
            solveMazeCell.addChild(solveGrid[i][j - 1]);
        }
    }

    private List<SolveMazeCell> generateSolution(SolveMazeCell destination) {
        List<SolveMazeCell> reverseSolution = new ArrayList<>();
        while (destination != null) {
            reverseSolution.add(destination);
            destination = destination.getParent();
        }

        Collections.reverse(reverseSolution);

        return reverseSolution;
    }
}
