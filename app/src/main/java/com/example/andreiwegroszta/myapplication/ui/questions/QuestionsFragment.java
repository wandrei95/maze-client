package com.example.andreiwegroszta.myapplication.ui.questions;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Category;
import com.example.andreiwegroszta.myapplication.entities.Question;

import java.util.List;

public class QuestionsFragment extends Fragment {
    public static final String CATEGORY_KEY = "CATEGORY";

    private RecyclerView rvQuestions;

    private QuestionsAdapter questionsAdapter;
    private QuestionSelectedListener listener;

    public QuestionsFragment() {
        // Required empty public constructor
    }

    public void setQuestionSelectedListener(QuestionSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_questions, container, false);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(CATEGORY_KEY)) {
            Category category = (Category) bundle.getSerializable(CATEGORY_KEY);

            if (category != null) {
                List<Question> questions = category.getQuestions();
                rvQuestions = view.findViewById(R.id.rv_questions);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                rvQuestions.setLayoutManager(layoutManager);
                questionsAdapter = new QuestionsAdapter(questions);
                rvQuestions.setAdapter(questionsAdapter);

                questionsAdapter.setOnQuestionClickListener(question -> {
                    if (listener != null) {
                        listener.onQuestionSelected(question);
                    }
                });
            }

        }

        return view;
    }

    public interface QuestionSelectedListener {
        void onQuestionSelected(Question question);
    }

}
