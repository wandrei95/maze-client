package com.example.andreiwegroszta.myapplication.maze_view.maze;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class MazeGenerator {

    private final MazeCell[][] grid;
    private final Stack<MazeCell> stack;
    private final int numberOfColumns;
    private final int numberOfRows;
    private MazeCell current;
    private int visitedCells;

    public MazeGenerator(int numberOfRows, int numberOfColumns) {
        this.numberOfColumns = numberOfColumns;
        this.numberOfRows = numberOfRows;
        grid = new MazeCell[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                grid[i][j] = new MazeCell(i, j);
            }
        }

        current = grid[0][0];
        stack = new Stack<>();
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public MazeCell[][] createMaze() {
        current.setVisited(true);
        while (visitedCells != numberOfRows * numberOfColumns - 1) {
            MazeCell neighbor = getUnvisitedNeighbor();
            if (neighbor != null) {
                visitedCells++;

                stack.push(current);

                removeWall(current, neighbor);

                current = neighbor;
                current.setVisited(true);
            } else if (!stack.isEmpty()) {
                current = stack.pop();
            }
        }
        return grid;
    }

    private void removeWall(MazeCell current, MazeCell neighbor) {
        int x = current.getX() - neighbor.getX();
        if (removeHorizontalBarriers(current, neighbor, x)) return;

        removeVerticalBarriers(current, neighbor);
    }

    private boolean removeVerticalBarriers(MazeCell current, MazeCell neighbor) {
        int y = current.getY() - neighbor.getY();
        if (y == 1) {
            current.setLeft(false);
            neighbor.setRight(false);
            return true;
        } else if (y == -1) {
            current.setRight(false);
            neighbor.setLeft(false);
            return true;
        }
        return false;
    }

    private boolean removeHorizontalBarriers(MazeCell current, MazeCell neighbor, int x) {
        if (x == 1) {
            current.setTop(false);
            neighbor.setBottom(false);
            return true;
        } else if (x == -1) {
            current.setBottom(false);
            neighbor.setTop(false);
            return true;
        }
        return false;
    }

    private MazeCell getUnvisitedNeighbor() {
        int x = current.getX();
        int y = current.getY();

        List<MazeCell> unvisitedNeighbours = new ArrayList<>();

        //top
        if (x - 1 >= 0 && !grid[x - 1][y].isVisited()) {
            unvisitedNeighbours.add(grid[x - 1][y]);
        }
        //bottom
        if (x + 1 < numberOfRows && !grid[x + 1][y].isVisited()) {
            unvisitedNeighbours.add(grid[x + 1][y]);
        }
        //left
        if (y - 1 >= 0 && !grid[x][y - 1].isVisited()) {
            unvisitedNeighbours.add(grid[x][y - 1]);
        }
        //right
        if (y + 1 < numberOfColumns && !grid[x][y + 1].isVisited()) {
            unvisitedNeighbours.add(grid[x][y + 1]);
        }

        if (unvisitedNeighbours.size() == 0) {
            return null;
        }

        int position = new Random().nextInt(unvisitedNeighbours.size());
        return unvisitedNeighbours.get(position);
    }

    public MazeCell[][] getGrid() {
        return grid;
    }
}
