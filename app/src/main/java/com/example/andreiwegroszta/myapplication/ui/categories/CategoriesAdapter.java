package com.example.andreiwegroszta.myapplication.ui.categories;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.andreiwegroszta.myapplication.R;
import com.example.andreiwegroszta.myapplication.entities.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {
    private final List<Category> categories;
    private OnCategoryClickListener listener;

    public CategoriesAdapter(List<Category> categories) {
        this.categories = new ArrayList<>(categories);
    }

    public CategoriesAdapter() {
        this.categories = new ArrayList<>();
    }

    private void setCategories(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll(categories);
    }

    public void setOnCategoryClickListener(OnCategoryClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.category_layout, viewGroup, false);
        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        Category category = categories.get(i);
        categoryViewHolder.tvCategoryName.setText(category.getName());
        categoryViewHolder.container.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCategoryClicked(category);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCategoryName;
        private RelativeLayout container;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_category_name);
            container = itemView.findViewById(R.id.container);
        }
    }

    interface OnCategoryClickListener {
        void onCategoryClicked(Category category);
    }
}
